/*
  RF24SensorNet
  A library for sending standard sensor and actuator
  messages between nodes connected using RF24Network.

  Handling for RGBW packets - RGBW lights and light faders.

  --
  Ken Auberry <kauberry@mac.com>

  Adapted from RF24SensorNet_rgb.cpp by
  Peter Hardy <peter@hardy.dropbear.id.au>
*/
#ifndef __RF24_SENSOR_NET_HSV_H__
#define __RF24_SENSOR_NET_HSV_H__

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

#include "RF24Network.h"
#include "RF24SensorNet.h"
#include "RF24SensorNet_types.h"

void RF24SensorNet::addHsvReadHandler(hsvReadHandler handler)
{
  _hsvReadHandler = handler;
}

void RF24SensorNet::addHsvWriteHandler(hsvWriteHandler handler)
{
  _hsvWriteHandler = handler;
}

void RF24SensorNet::addHsvRcvHandler(hsvRcvHandler handler)
{
  _hsvRcvHandler = handler;
}

bool RF24SensorNet::writeHsv(uint16_t toAddr, uint16_t id,
			     int hsv[3], uint32_t timer)
{
  pkt_hsv_t payload;
  payload.id = id;
  for (int i=0; i<3; i++) payload.hsv[i] = hsv[i];
  payload.timer = timer;

  bool ok = _write(toAddr, PKT_HSV+64, &payload);
  return ok;
}

bool RF24SensorNet::readHsv(uint16_t toAddr, uint16_t id)
{
  pkt_hsv_t payload;
  payload.id = id;
  for (int i=0; i<3; i++) payload.hsv[i] = 0;
  payload.timer = 0;

  bool ok = _write(toAddr, PKT_HSV+32, &payload);
  return ok;
}

bool RF24SensorNet::sendHsv(uint16_t toAddr, uint16_t id,
			     int hsv[3], uint32_t timer)
{
  pkt_hsv_t payload;
  payload.id = id;
  for (int i=0; i<3; i++) payload.hsv[i] = hsv[i];
  payload.timer = timer;
  // Serial.println(payload);
  bool ok = _write(toAddr, PKT_HSV, &payload);
  return ok;
}
#endif
