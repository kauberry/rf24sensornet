#include <SPI.h>
#include <RF24.h>
#include <RF24Network.h>
#include <RF24SensorNet.h>
#include <EEPROM.h>
#include <printf.h>
#include "math.h"
#define DEG_TO_RAD(X) (M_PI*(X)/180)

const int SERIAL_NUMBER_ADDRESS = 1023;

//RF24 Radio Configuration
int myAddr;
static int CE_PIN = 7;
static int CS_PIN = 8;
static int BASE_ADDR = 0;

RF24 radio(CE_PIN,CS_PIN);
RF24Network network(radio);
RF24SensorNet sensornet(network);

//Pin Definitions
int r = 3; //pin 3 Red PWM Output
int g = 5; //pin 5 Green PWM Output
int b = 6; //pin 6 Blue PWM Output
int w = 9; //pin 9 White PWM Output

int hue, saturation, brightness;

unsigned long prvserialtime;
unsigned long currenttime;
bool currentPowerState = false; //overall power state of the system on/off

void setup() {
  //Set up the output pins
  pinMode(r, OUTPUT); // red output
  pinMode(g, OUTPUT); // green output
  pinMode(b, OUTPUT); // blue output
  pinMode(w, OUTPUT); // white output

  hue        = EEPROM.read(7);
  saturation = EEPROM.read(8);
  brightness = EEPROM.read(9);

  myAddr     = EEPROM.read(SERIAL_NUMBER_ADDRESS);

  printf_begin();
  Serial.begin(115200);
  // while(!Serial){
  //   delay(2);
  // }

  Serial.println("RF24 RGBW Light Controller v1.2");
  Serial.print("My Address is : ");
  Serial.println(myAddr);

  //wake up the radio
  SPI.begin();
  radio.begin();
  radio.setRetries(5,15);

  network.begin(/*channel*/90, /*node_address*/myAddr);
  sensornet.begin();
  radio.printDetails();

  //Attach functions for turning the lights on and off
  sensornet.addSwitchReadHandler(readPowerState);
  sensornet.addSwitchWriteHandler(setPowerState);

  //Attach functions for handling HSV requests
  sensornet.addHsvReadHandler(readHSVValues);
  sensornet.addHsvWriteHandler(setHSVValues);

  setLEDsHSV(hue,saturation,brightness);
}

void loop(){
  sensornet.update();
}

void readPowerState(uint16_t fromAddr, uint16_t id){
  Serial.println("trying to read power state");
  bool state = getPowerState();
  uint8_t retry_counter = 0;
  //bool state = true;
  if(!sensornet.sendSwitch(BASE_ADDR, 0, state, 0) && retry_counter < 4){
    delay(50);
    retry_counter++;
    Serial.print("XMit failed => retry ");
    Serial.println(retry_counter);
    readPowerState(fromAddr, id);
  }
}

void setPowerState(uint16_t fromAddr, uint16_t id, bool state, uint32_t timer){
  Serial.print("Switching Lights ");
  if(state == true){
    Serial.print("on ");
  }else{
    Serial.print("off ");
  }
  Serial.println("");
  if(state == true){
    allon();
  }else{
    alloff();
  }
  //sensornet.sendSwitch(BASE_ADDR, 0, state, 0);
}

bool getPowerState(){
  Serial.println("Retrieving Power State");
  bool state = false;
  if(hue > 0 || saturation > 0 || brightness > 0){
    state = true;
  }
  return state;
}



void readHSVValues(uint16_t fromAddr, uint16_t id){
  int hsvValues[3];
  hsvValues[0] = hue;
  hsvValues[1] = saturation;
  hsvValues[2] = brightness;
  sensornet.sendHsv(fromAddr, 0, hsvValues, 0);
}

void setHSVValues(uint16_t fromAddr, uint16_t id, int hsv[3], uint32_t timer){
  setLEDsHSV(hsv[0],hsv[1],hsv[2]);
  sensornet.sendHsv(fromAddr, 0, hsv, 0);
}

//
char * levelsToSetString(int red, int grn, int blu, int wht){
  static char levelString[16];
  snprintf(levelString, 16, "%i|%i|%i|%i", red,grn,blu,wht);
  Serial.print("LevelString = ");
  Serial.println(levelString);
  return levelString;
}

void setLEDsHSV(int h, int s, int b){
  int rgbw[3];

  if(hue != h && saturation != s && brightness != b){
    EEPROM.write(7, h);
    EEPROM.write(8, s);
    EEPROM.write(9, b);
  }
  hue = h;
  saturation = s;
  brightness = b;


  float hf,sf,bf;
  hf = h;
  sf = s / 100.0;
  bf = b / 100.0;
  hsi2rgbw(hf,sf,bf,rgbw);
  Serial.print("HSV=");
  Serial.print(hue);
  Serial.print(",");
  Serial.print(saturation);
  Serial.print(",");
  Serial.print(brightness);
  Serial.print("  R=");
  Serial.print(rgbw[0]);
  Serial.print(" G=");
  Serial.print(rgbw[1]);
  Serial.print(" B=");
  Serial.print(rgbw[2]);
  Serial.print(" W=");
  Serial.println(rgbw[3]);
  setLEDs(rgbw[0],rgbw[1],rgbw[2],rgbw[3]);

}

void setLEDs(int red, int green, int blue, int white){
  static char levelString[16];
  snprintf(levelString, 16, "%i|%i|%i|%i", red,green,blue,white);
  analogWrite(r,red);
  analogWrite(g, green);
  analogWrite(b, blue);
  analogWrite(w, white);
}

void alloff(){
  Serial.println("Turning the Lights off!");
  EEPROM.write(7,hue);
  EEPROM.write(8,saturation);
  EEPROM.write(9,brightness);
  setLEDs(0,0,0,0);
}

void allon(){
  int hv,sv,vv;
  hv = hue; //EEPROM.read(7);
  sv = saturation; //EEPROM.read(8);
  vv = brightness; //EEPROM.read(9);
  Serial.println("Turning the Lights on!");
  Serial.print("HSV=");
  Serial.print(hue);
  Serial.print(",");
  Serial.print(saturation);
  Serial.print(",");
  Serial.println(brightness);
  setLEDsHSV(hv,sv,vv);

}

void hsi2rgbw(float H, float S, float I, int* rgbw) {
  int r, g, b, w;
  float cos_h, cos_1047_h;
  H = fmod(H,360); // cycle H around to 0-360 degrees
  H = 3.14159*H/(float)180; // Convert to radians.
  S = S>0?(S<1?S:1):0; // clamp S and I to interval [0,1]
  I = I>0?(I<1?I:1):0;

  if(H < 2.09439) {
    cos_h = cos(H);
    cos_1047_h = cos(1.047196667-H);
    r = S*255*I/3*(1+cos_h/cos_1047_h);
    g = S*255*I/3*(1+(1-cos_h/cos_1047_h));
    b = 0;
    w = 255*(1-S)*I;
  } else if(H < 4.188787) {
    H = H - 2.09439;
    cos_h = cos(H);
    cos_1047_h = cos(1.047196667-H);
    g = S*255*I/3*(1+cos_h/cos_1047_h);
    b = S*255*I/3*(1+(1-cos_h/cos_1047_h));
    r = 0;
    w = 255*(1-S)*I;
  } else {
    H = H - 4.188787;
    cos_h = cos(H);
    cos_1047_h = cos(1.047196667-H);
    b = S*255*I/3*(1+cos_h/cos_1047_h);
    r = S*255*I/3*(1+(1-cos_h/cos_1047_h));
    g = 0;
    w = 255*(1-S)*I;
  }

  rgbw[0]=r;
  rgbw[1]=g;
  rgbw[2]=b;
  rgbw[3]=w;

}
