#include <SPI.h>
#include <RF24.h>
#include <RF24Network.h>
#include <RF24SensorNet.h>
#include <EEPROM.h>
#include <printf.h>
// #include "math.h"
// #define DEG_TO_RAD(X) (M_PI*(X)/180)

const int SERIAL_NUMBER_ADDRESS = 1023;

//RF24 Radio Configuration
int myAddr;
static uint16_t CE_PIN = 7;
static uint16_t CS_PIN = 8;
static uint16_t BASE_ADDR = 0;

RF24 radio(CE_PIN,CS_PIN);
RF24Network network(radio);
RF24SensorNet sensornet(network);




//Pin Definitions
int r = 3; //pin 3 Red PWM Output
int g = 5; //pin 5 Green PWM Output
int b = 6; //pin 6 Blue PWM Output
int w = 9; //pin 9 White PWM Output

//current output levels
int redlev; //current Red level
int grnlev; //current Green level
int blulev; //current Blue level
int whtlev; //current White level

//desired output levels
int redtgt; //target Red level
int grntgt; //target Green level
int blutgt; //target Blue level
int whttgt; //target White level

//wake up values from EEPROM
int redwake; //waking Red level
int grnwake; //waking Green level
int bluwake; //waking Blue level
int whtwake; //waking White level

int ramp;
int stay; // delay in seconds to stay on each color in cycle mode
int rate; // rate of color cycle
int cyclego = 0;
int cyclepause;
int mode;

bool switchState; //flag to turn device on/off

int serialtime = 45; // how often serial checks are made
unsigned long prvserialtime;
unsigned long currenttime;

static unsigned long reportingInterval = 10000;
bool currentPowerState;
char currentLEDLevels[3];



void setup() {
  //Set up the output pins
  pinMode(r, OUTPUT); // red output
  pinMode(g, OUTPUT); // green output
  pinMode(b, OUTPUT); // blue output
  pinMode(w, OUTPUT); // white output



  //read values in from the local EEPROM
  ramp = EEPROM.read(1);
  rate = EEPROM.read(2);
  redwake = EEPROM.read(3);
  grnwake = EEPROM.read(4);
  bluwake = EEPROM.read(5);
  whtwake = EEPROM.read(6);
  mode = EEPROM.read(7);
  cyclego = EEPROM.read(8);
  cyclepause = EEPROM.read(9);
  if (rate == 255) {
    rate = 4;
    EEPROM.write(2, rate);
  }
  if (ramp == 255) {
    ramp = 4;
    EEPROM.write(1, ramp);
  }
  if (mode == 255) {
    mode = 0;
    EEPROM.write(7, mode);
  }
  if (cyclego == 255) {
    cyclego = 0;
    EEPROM.write(8, cyclego);
  }
  if (cyclepause == 255) {
    cyclepause = 1;
    EEPROM.write(9, cyclepause);
  }



  prvserialtime = 0;

  myAddr = EEPROM.read(SERIAL_NUMBER_ADDRESS);

  //setting up default values if the EEPROM is empty?
  if (rate == 255) {
    rate = 4;
    EEPROM.write(2, rate);
  }
  if (ramp == 255) {
    ramp = 4;
    EEPROM.write(1, ramp);
  }
  if (mode == 255) {
    mode = 0;
    EEPROM.write(7, mode);
  }
  if (cyclego == 255) {
    cyclego = 0;
    EEPROM.write(8, cyclego);
  }
  if (cyclepause == 255) {
    cyclepause = 1;
    EEPROM.write(9, cyclepause);
  }
  printf_begin();
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
  Serial.println("RF24 RGBW Light Controller v1.0");
  Serial.print("My Address is : ");
  Serial.println(myAddr);

  //wake up the radio
  SPI.begin();
  radio.begin();
  network.begin(/*channel*/90, /*node_address*/myAddr);
  sensornet.begin();

  radio.printDetails();


  //Attach a function for the light settings read/write callbacks
  sensornet.addRgbReadHandler(readLEDValues);
  sensornet.addRgbWriteHandler(setLEDValues);

  //Attach functions for turning the lights on and off
  sensornet.addSwitchReadHandler(readPowerState);
  sensornet.addSwitchWriteHandler(setPowerState);

  //set the default startup values for the light
  redlev = redwake;
  grnlev = grnwake;
  blulev = bluwake;
  whtlev = whtwake;
  red(redlev);
  green(grnlev);
  blue(blulev);
  white(whtlev);
}

void readLEDValues(uint16_t fromAddr, uint16_t id){
  Serial.println("Attempting to Read LED Values");
  char rgb[3];
  getLEDValues();
  Serial.println(rgb);
  //sensornet.sendRgbw(BASE_ADDR, 0, currentLEDLevels, 0);
}

void setLEDValues(uint16_t fromAddr, uint16_t id, char rgb[3], uint32_t timer){
  Serial.print("Setting RGBW values! (R=");
  Serial.print(rgb[0]);
  red(rgb[0]);;
  Serial.print(" G=");
  Serial.print(rgb[1]);
  green(rgb[1]);
  Serial.print(" B=");
  Serial.print(rgb[2]);
  blue(rgb[2]);
//  Serial.print(" W=");
//  Serial.print(rgb[3]);
//  white(rgbw[3]);
  Serial.println(")");
  sensornet.sendRgb(BASE_ADDR, 0, rgb, 0);
}

void readPowerState(uint16_t fromAddr, uint16_t id){
  bool state = getPowerState();
  sensornet.sendSwitch(BASE_ADDR, 0, state, 0);
}

void setPowerState(uint16_t fromAddr, uint16_t id, bool state, uint32_t timer){
  Serial.print("Switching Lights ");
  if(state == true){
    Serial.print("on ");
  }else{
    Serial.print("off ");
  }
  Serial.println("");
  if(state == true){
    allon();
  }else{
    alloff();
  }
  sensornet.sendSwitch(BASE_ADDR, 0, state, 0);
}

bool getPowerState(){
  Serial.println("Retrieving Power State");
  bool state = false;
  if(whtlev > 0 || grnlev > 0 || blulev > 0 || redlev > 0){
    state = true;
  }
  return state;
}

void getLEDValues(){
  Serial.println("Retrieving LED Settings");
  char rgb[3];
  rgb[0] = redlev;
  rgb[1] = grnlev;
  rgb[2] = blulev;
//  rgbw[3] = whtlev;
  for (int i=0; i<3; i++) currentLEDLevels[i] = rgb[i];
  sensornet.sendRgb(BASE_ADDR, 0, rgb, 0);
}

void alloff(){
  Serial.println("Turning the Lights off!");
  EEPROM.write(23, redlev);
  EEPROM.write(24, grnlev);
  EEPROM.write(25, blulev);
  EEPROM.write(26, whtlev);
  red(0);
  blue(0);
  green(0);
  white(0);
}

void allon(){
  redlev = EEPROM.read(23);
  grnlev = EEPROM.read(24);
  blulev = EEPROM.read(25);
  whtlev = EEPROM.read(26);
  Serial.print("Turning the Lights on! (R=");
  Serial.print(redlev);
  Serial.print(" G=");
  Serial.print(grnlev);
  Serial.print(" B=");
  Serial.print(blulev);
  Serial.print(" W=");
  Serial.print(whtlev);
  Serial.println(")");
  red(redlev);
  blue(blulev);
  green(grnlev);
  white(whtlev);
}



void loop() {
  sensornet.update();
  currenttime = millis();
  if(currenttime - prvserialtime > reportingInterval){
    currentPowerState = getPowerState();
    getLEDValues();
    sensornet.sendSwitch(BASE_ADDR, 0, currentPowerState, 0);
    sensornet.sendRgb(BASE_ADDR, 0, currentLEDLevels, 0);
    prvserialtime = currenttime;
  }

}



void white(int whttgt)  { // white control
  if (whtlev < whttgt) { // if the desired level is higher than the current level
    for (int x = whtlev; x <= whttgt; x++) { // start ramping at the current level and brighten to the desired level
      analogWrite(w, x); // write the value to the pin
      delay(ramp); // delay for 8ms
    }
  }
  if (whtlev > whttgt) { // if the desired level is lower than the current level
    for (int x = whtlev; x >= whttgt; x--) { // start ramping at the current level and darken or dim to the desired level
      analogWrite(w, x); // write the value to the pin
      delay(ramp); // delay 8ms
    }
  }
  whtlev = whttgt; // the desired level is now the current level
  EEPROM.write(6, whtlev);
}

void red(int redtgt)  { // red control
  if (redlev < redtgt) {
    for (int x = redlev; x <= redtgt; x++) {
      analogWrite(r, x);
      delay(ramp);
    }
  }
  if (redlev > redtgt) {
    for (int x = redlev; x >= redtgt; x--) {
      analogWrite(r, x);
      delay(ramp);
    }
  }
  redlev = redtgt;
  EEPROM.write(3, redlev);
}

void green(int grntgt)  { // green control
  if (grnlev < grntgt) {
    for (int x = grnlev; x <= grntgt; x++) {
      analogWrite(g, x);
      delay(ramp);
    }
  }
  if (grnlev > grntgt) {
    for (int x = grnlev; x >= grntgt; x--) {
      analogWrite(g, x);
      delay(ramp);
    }
  }
  grnlev = grntgt;
  EEPROM.write(4, grnlev);
}

void blue(int blutgt)  { // blue control
  if (blulev < blutgt) {
    for (int x = blulev; x <= blutgt; x++) {
      analogWrite(b, x);
      delay(ramp);
    }
  }
  if (blulev > blutgt) {
    for (int x = blulev; x >= blutgt; x--) {
      analogWrite(b, x);
      delay(ramp);
    }
  }
  blulev = blutgt;
  EEPROM.write(5, blulev);
}

//predefined color functions
void cyan() {
  blue(255);
  green(255);
  red(0);
}
void gold() {
  red(255);
  green(255);
  blue(0);
}
void magenta() {
  blue(255);
  red(255);
  green(0);
}
void pink() {
  red(255);
  blue(51);
  green(0);
}
void violet() {
  blue(255);
  red(166);
  green(0);
}
void ltgreen() {
  red(0);
  green(255);
  blue(25);
}
void ltblue() {
  blue(255);
  green(191);
  red(0);
}
void orange() {
  red(255);
  green(38);
  blue(0);
}
void rgbwhite() {
  red(255);
  green(255);
  blue(255);
}
void rgbww() {
  blue(12);
  green(115);
  red(242);
}

//void cycle() { // cycle routine
//  if (cyclego == 1) {
//    serialchk();
//    for(i = 0 ; i <= 255; i+=1) {  // blue on
//      if (cyclego == 1) {
//        analogWrite(b, i);
//        buttons();
//        delay(rate);
//        blulev = i;
//      }
//    }
//  }
//  delay(stay);
//  if (cyclego == 1) {
//    serialchk();
//    for(i = 255 ; i >= 0; i-=1) { // red off
//      if (cyclego == 1) {
//        analogWrite(r, i);
//        buttons();
//        delay(rate);
//        redlev = i;
//      }
//    }
//  }
//  delay(stay);
//  if (cyclego == 1) {
//    serialchk();
//    for(i = 0 ; i <= 255; i+=1) { // green on
//      if (cyclego == 1) {
//        analogWrite(g, i);
//        buttons();
//        delay(rate);
//        grnlev = i;
//      }
//    }
//  }
//  delay(stay);
//  if (cyclego == 1) {
//    serialchk();
//    for(i = 255 ; i >= 0; i-=1) { // blue off
//      if (cyclego == 1) {
//        analogWrite(b, i);
//        buttons();
//        delay(rate);
//        blulev = i;
//      }
//    }
//  }
//  delay(stay);
//  if (cyclego == 1) {
//    serialchk();
//    for(i = 0 ; i <= 255; i+=1) { // red on
//      if (cyclego == 1) {
//        analogWrite(r, i);
//        buttons();
//        delay(rate);
//        redlev = i;
//      }
//    }
//  }
//  delay(stay);
//  if (cyclego == 1) {
//    serialchk();
//    for(i = 255 ; i >= 0; i-=1) { // green off
//      if (cyclego == 1) {
//        analogWrite(g, i);
//        buttons();
//        delay(rate);
//        grnlev = i;
//      }
//    }
//  }
//  delay(stay);
//  /*
//  EEPROM.write(6, whtlev);
//  EEPROM.write(3, redlev);
//  EEPROM.write(4, grnlev);
//  EEPROM.write(5, blulev);
//  */
//}
