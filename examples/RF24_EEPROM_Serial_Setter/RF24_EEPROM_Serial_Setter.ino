#include <EEPROM.h>

const int SERIAL_NUMBER_ADDRESS = 1023;
const byte SERIAL_NUMBER = 42;
void setup ()
  {
  // don't do it if it has been done before
  if (EEPROM.read(SERIAL_NUMBER_ADDRESS) != SERIAL_NUMBER)
    EEPROM.write(SERIAL_NUMBER_ADDRESS, SERIAL_NUMBER);
  }  // end of setup

void loop () { }
