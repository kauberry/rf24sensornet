#include <SPI.h>
#include <RF24.h>
#include <RF24Network.h>
#include <RF24SensorNet.h>
#include <EEPROM.h>
#include <printf.h>
// #include "math.h"
// #define DEG_TO_RAD(X) (M_PI*(X)/180)

const int SERIAL_NUMBER_ADDRESS = 1023;

//RF24 Radio Configuration
int myAddr;
static uint16_t CE_PIN = 7;
static uint16_t CS_PIN = 8;
static uint16_t BASE_ADDR = 0;

RF24 radio(CE_PIN,CS_PIN);
RF24Network network(radio);
RF24SensorNet sensornet(network);

//Pin Definitions
int r = 3; //pin 3 Red PWM Output
int g = 5; //pin 5 Green PWM Output
int b = 6; //pin 6 Blue PWM Output
int w = 9; //pin 9 White PWM Output

//current output levels
int redlev; //current Red level
int grnlev; //current Green level
int blulev; //current Blue level
int whtlev; //current White level

//desired output levels
int redtgt; //target Red level
int grntgt; //target Green level
int blutgt; //target Blue level
int whttgt; //target White level

//wake up values from EEPROM
int redwake; //waking Red level
int grnwake; //waking Green level
int bluwake; //waking Blue level
int whtwake; //waking White level

// char setstring[];
unsigned long prvserialtime;
unsigned long currenttime;

int ramp;
int defaultRamp = 50;    //speed at which to ramp hue/luminance changes
bool switchState; //flag to turn device on/off

int reportingInterval = 30000;  //time between periodic checkins (in ms)

bool currentPowerState = false; //overall power state of the system on/off
char * currentLEDLevels = "0|0|0|0";

void setup() {
  //Set up the output pins
  pinMode(r, OUTPUT); // red output
  pinMode(g, OUTPUT); // green output
  pinMode(b, OUTPUT); // blue output
  pinMode(w, OUTPUT); // white output



  //read values in from the local EEPROM
  //set the ramping rate
  ramp = EEPROM.read(1);
  Serial.print("Stored ramp value = ");
  Serial.println(ramp);
  if(ramp == 255){
    ramp = defaultRamp;
    Serial.println("Stored ramp value is invalid. Setting from default");
  }

  redwake = EEPROM.read(3);
  grnwake = EEPROM.read(4);
  bluwake = EEPROM.read(5);
  whtwake = EEPROM.read(6);
  myAddr  = EEPROM.read(SERIAL_NUMBER_ADDRESS);

  printf_begin();
  Serial.begin(115200);
  while(!Serial){
    delay(10);
  }

  Serial.println("RF24 RGBW Light Controller v1.0");
  Serial.print("My Address is : ");
  Serial.println(myAddr);

  //wake up the radio
  SPI.begin();
  radio.begin();
  network.begin(/*channel*/90, /*node_address*/myAddr);
  sensornet.begin();

  radio.printDetails();

  //Attach a function for the RGB only read/write callbacks
  // sensornet.addRgbReadHandler(readRGBValues);
  // sensornet.addRgbWriteHandler(setRGBValues);

  //Attach a function for the RGBW read/write callbacks
  sensornet.addRgbwReadHandler(readLEDValues);
  sensornet.addRgbwWriteHandler(setLEDValues);

  //Attach functions for turning the lights on and off
  // sensornet.addSwitchReadHandler(readPowerState);
  // sensornet.addSwitchWriteHandler(setPowerState);

  redlev = redwake;
  grnlev = grnwake;
  blulev = bluwake;
  whtlev = whtwake;

  char * setString = levelsToSetString(redlev,grnlev,blulev,whtlev);
  setLevelsFromString(setString);

}

char * setString;
void loop(){
  sensornet.update();
  currenttime = millis();
  if(currenttime - prvserialtime > reportingInterval){
//    currentPowerState = getPowerState();
    readLEDValues(0, 0);
    sensornet.sendSwitch(BASE_ADDR, 0, currentPowerState, 0);
    sensornet.sendRgbw(BASE_ADDR, 0, currentLEDLevels, 0);
    prvserialtime = currenttime;
  }
  delay(5000);
  setString = "100|150|200|50";
  setLevelsFromString(setString);
  delay(5000);
  setString = "200|50|20|150";
  setLevelsFromString(setString);
}

void readLEDValues(uint16_t fromAddr, uint16_t id){
  Serial.println("Attempting to Read LED Values");
  char rgbw[4];
  rgbw[0] = redlev;
  rgbw[1] = grnlev;
  rgbw[2] = blulev;
  rgbw[3] = whtlev;
  Serial.println(levelsToSetString(redlev,grnlev,blulev,whtlev));
  sensornet.sendRgbw(BASE_ADDR, 0, currentLEDLevels, 0);
}

void setLEDValues(uint16_t fromAddr, uint16_t id, char rgbw[4], uint32_t timer){
  Serial.print("Setting RGBW values! (R=");
  Serial.print(rgbw[0]);
  Serial.print(" G=");
  Serial.print(rgbw[1]);
  Serial.print(" B=");
  Serial.print(rgbw[2]);
  Serial.print(" W=");
  Serial.print(rgbw[3]);
  Serial.println(")");

  setLevels(rgbw[0],rgbw[1],rgbw[2],rgbw[3]);
  sensornet.sendRgb(BASE_ADDR, 0, rgbw, 0);
}


char * levelsToSetString(uint16_t red, uint16_t grn, uint16_t blu, uint16_t wht){
  red = constrain(red, 0, 254);
  grn = constrain(grn, 0, 254);
  blu = constrain(blu, 0, 254);
  wht = constrain(wht, 0, 254);
  static char levelString[15];
  snprintf(levelString, 15, "%i|%i|%i|%i", red,grn,blu,wht);
  return levelString;
}

void setLevelsFromString(char* setString){
  char * token;
  uint16_t red, grn, blu, wht;
  char * redString, grnString, bluString, whtString;
  Serial.println(setString);
  token = strtok(setString,"|");
  red = atoi(token);
  Serial.print("R:");
  Serial.print(red);
  token = strtok(NULL,"|");
  grn = atoi(token);
  Serial.print(" G:");
  Serial.print(grn);
  token = strtok(NULL,"|");
  blu = atoi(token);
  Serial.print(" B:");
  Serial.print(blu);
  token = strtok(NULL,"|");
  wht = atoi(token);
  Serial.print(" W:");
  Serial.println(wht);
  setLevels(red,grn,blu,wht);
}

void setLevels(uint16_t redtgt, uint16_t grntgt, uint16_t blutgt, uint16_t whttgt){
  char rgbwtgt[] = {redtgt,grntgt,blutgt,whttgt};
  char rgbwlev[] = {redlev,grnlev,blulev,whtlev};
  int16_t redValue, grnValue, bluValue, whtValue;

  int16_t delayVal = ramp;
  int16_t steps = 16;

  int16_t redDiff = rgbwtgt[0] - rgbwlev[0];
  int16_t grnDiff = rgbwtgt[1] - rgbwlev[1];
  int16_t bluDiff = rgbwtgt[2] - rgbwlev[2];
  int16_t whtDiff = rgbwtgt[3] - rgbwlev[3];

  for (int16_t i=0 ;  i < steps - 1 ; ++i){
    redValue = rgbwlev[0] + (redDiff * i / steps);
    grnValue = rgbwlev[1] + (grnDiff * i / steps);
    bluValue = rgbwlev[2] + (bluDiff * i / steps);
    whtValue = rgbwlev[3] + (whtDiff * i / steps);

    setLEDs(redValue,grnValue,bluValue,whtValue);
    delay(delayVal);
  }
  setLEDs(redtgt,grntgt,blutgt,whttgt);
  EEPROM.write(3, redtgt);
  EEPROM.write(4, grntgt);
  EEPROM.write(5, blutgt);
  EEPROM.write(6, whttgt);
}

void setLEDs(int16_t red, int16_t green, int16_t blue, int16_t white){
  analogWrite(r,red);
  analogWrite(g, green);
  analogWrite(b, blue);
  analogWrite(w, white);
}
