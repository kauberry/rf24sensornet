/*
  RF24SensorNet
  A library for sending standard sensor and actuator
  messages between nodes connected using RF24Network.

  Handling for RGBW packets - RGBW lights and light faders.

  --
  Ken Auberry <kauberry@mac.com>

  Adapted from RF24SensorNet_rgb.cpp by
  Peter Hardy <peter@hardy.dropbear.id.au>
*/
#ifndef __RF24_SENSOR_NET_RGBW_H__
#define __RF24_SENSOR_NET_RGBW_H__

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

#include "RF24Network.h"
#include "RF24SensorNet.h"
#include "RF24SensorNet_types.h"

void RF24SensorNet::addRgbwReadHandler(rgbwReadHandler handler)
{
  _rgbwReadHandler = handler;
}

void RF24SensorNet::addRgbwWriteHandler(rgbwWriteHandler handler)
{
  _rgbwWriteHandler = handler;
}

void RF24SensorNet::addRgbwRcvHandler(rgbwRcvHandler handler)
{
  _rgbwRcvHandler = handler;
}

bool RF24SensorNet::writeRgbw(uint16_t toAddr, uint16_t id,
			     uint32_t rgbw, uint32_t timer)
{
  pkt_rgbw_t payload;
  payload.id = id;
  payload.rgbw = rgbw;
  payload.timer = timer;

  bool ok = _write(toAddr, PKT_RGBW+64, &payload);
  return ok;
}

bool RF24SensorNet::readRgbw(uint16_t toAddr, uint16_t id)
{
  pkt_rgbw_t payload;
  payload.id = id;
  payload.rgbw = 0;
  payload.timer = 0;

  bool ok = _write(toAddr, PKT_RGBW+32, &payload);
  return ok;
}

bool RF24SensorNet::sendRgbw(uint16_t toAddr, uint16_t id,
			     uint32_t rgbw, uint32_t timer)
{
  pkt_rgbw_t payload;
  payload.id = id;
  //TODO: convert to uint32_t
  payload.rgbw = rgbw;
  payload.timer = timer;
  // Serial.println(payload);
  bool ok = _write(toAddr, PKT_RGBW, &payload);
  return ok;
}
#endif
